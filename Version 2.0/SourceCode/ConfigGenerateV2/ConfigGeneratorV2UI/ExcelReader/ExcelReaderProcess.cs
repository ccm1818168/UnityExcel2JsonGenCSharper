﻿//
//    Copyright 2016 KeyleXiao.
//    Contact to Me : Keyle_xiao@hotmail.com 
//
//   	Licensed under the Apache License, Version 2.0 (the "License");
//   	you may not use this file except in compliance with the License.
//   	You may obtain a copy of the License at
//
//   		http://www.apache.org/licenses/LICENSE-2.0
//
//   		Unless required by applicable law or agreed to in writing, software
//   		distributed under the License is distributed on an "AS IS" BASIS,
//   		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   		See the License for the specific language governing permissions and
//   		limitations under the License.
//
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.IO;
using IGeneratorContract;
using ConfigGeneratorV2UI.Config;
using System.Collections.Generic;

namespace ConfigGeneratorV2UI.ExcelReader
{
    public class ExcelReaderProcess : IDisposable
    {
        /// <summary>
        /// 从第x行开始读取
        /// </summary>
        public int startReadIndex = 1;

        /// <summary>
        /// 属性头从x行读取 没有读到的统一按照string处理
        /// </summary>
        public int attributeReadIndex = 2;


        private string fileName = null;
        private IWorkbook workbook = null;
        private FileStream fs = null;
        public int cellCount;
        public int rowCount;
        List<IExcelModel> ExcelInfos { get; set; }

        public ExcelReaderProcess(string fileName)
        {
            this.fileName = fileName;
            ExcelInfos = new List<IExcelModel>();
        }

        public List<IExcelModel> GetExcelInfo(out string msg)
        {
            msg = "";

            fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);

                if (fileName.IndexOf(".xlsx") > 0)
                    workbook = new XSSFWorkbook(this.fs);
                else if (fileName.IndexOf(".xls") > 0)
                    workbook = new HSSFWorkbook(this.fs);
                else return null;

                //get all sheets
                string tempOutputMsg = string.Empty;
                for (int i = 0; i < workbook.NumberOfSheets; i++)
                {
                    tempOutputMsg = string.Empty;
                    var excelinfo = GetExcelInfoBySheetName(i,workbook.GetSheetAt(i),out tempOutputMsg);

                if (excelinfo == null)
                {
                    msg = string.Format("{0}\n{1}", msg, tempOutputMsg);
                    continue;
                }

                    
                    excelinfo.WorkBookName = Path.GetFileNameWithoutExtension(fileName);//Set WorkBook Name
                    ExcelInfos.Add(excelinfo);
                }

                return ExcelInfos;
            

            return ExcelInfos;
        }


        //Get Sheet Info 
        public IExcelModel GetExcelInfoBySheetName(int sheetIndex,ISheet sheet, out string msg) {
            msg = string.Empty;

            var ExcelInfo = new ExcelModel();
            ExcelInfo.SheetName = sheet.SheetName;

            if (sheet == null)
            {
                msg = string.Format("Current sheet is null , index = {0}", sheetIndex);
                return null;
            }
            int num = 0;

            //This row is title row 
            IRow row = sheet.GetRow(0);


            if (row == null)
            {
                msg = string.Format("Currnet sheet line zero is null ,Sheet = {0}",sheet.SheetName);
                return null;
            }

            cellCount = row.LastCellNum;
            rowCount = sheet.LastRowNum;

            //Get title type is string
            for (int i = (int)row.FirstCellNum; i < this.cellCount; i++)
            {
                ICell cell = row.GetCell(i);
                if (cell != null)
                {
                    string stringCellValue = cell.StringCellValue;
                    if (!string.IsNullOrWhiteSpace(stringCellValue))
                    {
                        if (ExcelInfo.ColumnName.Contains(stringCellValue))
                        {
                            msg = string.Format("The List Has The Same Column Title ,Name '{0}'  Sheet = ", stringCellValue,sheet.SheetName);
                            return null;
                        }
                        ExcelInfo.ColumnName.Add(stringCellValue);
                    }
                    else
                        ExcelInfo.ColumnName.Add(string.Format("Column{0}", i));
                }
            }

            //num = sheet.FirstRowNum + 1;
            row = sheet.GetRow(attributeReadIndex);// 1
            if (row == null)
            {
                msg = string.Format("Currnet sheet line 1 is null ,Sheet = {0}", sheet.SheetName);
                return null;
            }

            //Get types as list
            for (int i = row.FirstCellNum; i < cellCount; i++)
            {
                ICell cell = row.GetCell(i);
                if (cell != null)
                {
                    string stringCellValue = cell.StringCellValue.ToUpper().Trim();
                    if (!string.IsNullOrEmpty(stringCellValue))
                    {
                        switch (stringCellValue)
                        {
                            case "STRING":
                                ExcelInfo.ColumnType.Add(typeof(string));
                                break;
                            case "FLOAT":
                                ExcelInfo.ColumnType.Add(typeof(float));
                                break;
                            case "INT":
                                ExcelInfo.ColumnType.Add(typeof(int));
                                break;
                            case "STRING[]":
                                ExcelInfo.ColumnType.Add(typeof(string[]));
                                break;
                            case "FLOAT[]":
                                ExcelInfo.ColumnType.Add(typeof(float[]));
                                break;
                            case "INT[]":
                                ExcelInfo.ColumnType.Add(typeof(int[]));
                                break;
                            default:
                                ExcelInfo.ColumnType.Add(typeof(string));
                                msg = string.Format("Cant find the Type '{0}'  , Sheet = {1} ", stringCellValue,sheet.SheetName);
                                break;
                        }
                    }
                    else
                    {
                        ExcelInfo.ColumnType.Add(typeof(string));
                        msg = string.Format("You need one row of column attribute  , Sheet = {0} ", sheet.SheetName);
                    }
                }
            }

            num = startReadIndex;//2

            for (int i = num; i <= this.rowCount; i++)
            {
                IRow row2 = sheet.GetRow(i);
                if (row2 != null)
                {
                    if ((int)row2.FirstCellNum >= cellCount)
                        continue;

                    List<string> items = new List<string>();
                    for (int j = (int)row2.FirstCellNum; j < this.cellCount; j++)
                    {
                        if (row2.GetCell(j) != null)
                        {
                            items.Add(row2.GetCell(j).ToString());
                        }
                    }
                    ExcelInfo.Rows.Add(i, items);
                }
            }

      

            return ExcelInfo;
        }


        public void Dispose()
        {
            if (this.fs != null)
            {
                this.fs.Close();
            }
            fs = null;
            GC.SuppressFinalize(this);
        }
    }
}
