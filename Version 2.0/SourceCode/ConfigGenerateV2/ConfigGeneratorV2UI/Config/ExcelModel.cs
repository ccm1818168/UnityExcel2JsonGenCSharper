﻿//
//    Copyright 2016 KeyleXiao.
//    Contact to Me : Keyle_xiao@hotmail.com 
//
//   	Licensed under the Apache License, Version 2.0 (the "License");
//   	you may not use this file except in compliance with the License.
//   	You may obtain a copy of the License at
//
//   		http://www.apache.org/licenses/LICENSE-2.0
//
//   		Unless required by applicable law or agreed to in writing, software
//   		distributed under the License is distributed on an "AS IS" BASIS,
//   		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   		See the License for the specific language governing permissions and
//   		limitations under the License.
//
using System;
using System.Collections.Generic;
using IGeneratorContract;

namespace ConfigGeneratorV2UI.Config
{
    public class ExcelModel : IExcelModel
    {
        public ExcelModel()
        {
            SheetName = string.Empty;
            WorkBookName = string.Empty;
            ColumnType = new List<Type>();
            ColumnName = new List<string>();
            Rows = new Dictionary<int, List<string>>();
        }

        public string WorkBookName { get; set; }
        public string SheetName { get; set; }

        #region IExcelModel Members

        public List<Type> ColumnType
        {
            get;
            set;
        }

        public List<string> ColumnName
        {
            get;
            set;
        }

        public Dictionary<int, List<string>> Rows
        {
            get;
            set;
        }

        #endregion
    }
}