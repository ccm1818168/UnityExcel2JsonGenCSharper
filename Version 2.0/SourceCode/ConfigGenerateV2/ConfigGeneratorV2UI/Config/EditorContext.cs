﻿//
//    Copyright 2016 KeyleXiao.
//    Contact to Me : Keyle_xiao@hotmail.com 
//
//   	Licensed under the Apache License, Version 2.0 (the "License");
//   	you may not use this file except in compliance with the License.
//   	You may obtain a copy of the License at
//
//   		http://www.apache.org/licenses/LICENSE-2.0
//
//   		Unless required by applicable law or agreed to in writing, software
//   		distributed under the License is distributed on an "AS IS" BASIS,
//   		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   		See the License for the specific language governing permissions and
//   		limitations under the License.
//
using System;
using System.Collections.Generic;
using System.Linq;
using IGeneratorContract;
//using System.Configuration;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace ConfigGeneratorV2UI.Config
{
    public class EditorContext: IContext
    {

       Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        private EditorContext()
        {
            //Init here
            if (ExcelInfos == null)
                ExcelInfos = new Dictionary<IExcelModel, bool>();

            AUTHOR_INFO = GetConfig("AUTHOR_INFO");
            string content = GetConfig("SETTING");

            if ( !string.IsNullOrEmpty(content))
                CustomSetting = JsonConvert.DeserializeObject<UserSetting>(content);

            if (string.IsNullOrEmpty(content)) CustomSetting = new UserSetting() { DefaultLoadExcelPathList = new List<string>(),DefaultExportPath =string.Empty };
        }

        public void UpdateAppSetting()
        {
            SetConfig("SETTING", JsonConvert.SerializeObject(CustomSetting));
        }


        public  string GetConfig(string key)
        {
            string _value = string.Empty;

            if (config.AppSettings.Settings[key] != null)
            {
                _value = config.AppSettings.Settings[key].Value;
            }
            return _value;
        }

        public  void SetConfig(string key, string value)
        {
            if (config.AppSettings.Settings[key] != null)
                config.AppSettings.Settings[key].Value = value;
            else
                config.AppSettings.Settings.Add(key, value);

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }


        private static EditorContext instance;
        public static EditorContext Instance
        {
            get { return instance ?? (instance = new EditorContext()); }
        }

        public Dictionary<IExcelModel, bool> ExcelInfos { get; set; }

        public string AUTHOR_INFO { get; set; }

        //public string AutoLoadExcels { get; set; }
        public  UserSetting CustomSetting  { get; set; }

     // public List<string> AutoLoadExcelConfig { get; set; }

        public IExcelModel SearchExcelInfosWithSheetName(string sheetName)
        {
            foreach (var item in ExcelInfos.Keys)
            {
                if (item.SheetName == sheetName)
                {
                    return item;
                }
            }
            return null;
        }
    }
}

public class AutoLoadConfig
{
    public string ExcelPath { get; set; }
    public List<string> SheetName { get; set; }
}