﻿//
//    Copyright 2018 KeyleXiao.
//    Contact to Me : Keyle_xiao@hotmail.com 
//
//   	Licensed under the Apache License, Version 2.0 (the "License");
//   	you may not use this file except in compliance with the License.
//   	You may obtain a copy of the License at
//
//   		http://www.apache.org/licenses/LICENSE-2.0
//
//   		Unless required by applicable law or agreed to in writing, software
//   		distributed under the License is distributed on an "AS IS" BASIS,
//   		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   		See the License for the specific language governing permissions and
//   		limitations under the License.
//

using System.Collections.Generic;
using IGeneratorContract;
using System.Linq;
using System.IO;


namespace GeneratorFilesWithSpecialSheet
{
    public class GeneFilesWithSpecialSheet : IPluginContract
    {
        [MenuItem("Files/GenFileWithContent")]
        public void GenJsonFile()
        {
            foreach (var item in Context.ExcelInfos)
            {
                if (!item.Value)
                    continue;

                string temp_content = string.Empty;

                List<List<string>> rows = item.Key.Rows.Values.ToList();

                for (int i = 0; i < rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(rows[i][0]))
                        continue;

                    string path = Path.Combine(Context.CustomSetting.DefaultExportPath, rows[i][0] + ".txt");
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }

                    string content = rows[i][1].Replace('\n', ' ').Replace('!', ' ').Replace('?', ' ').Replace('\"', ' ').Replace('、',' ').Replace('（', ' ').Replace('）', ' ').Replace('。',' ')
                        .Replace('！', ' ').Replace('～', ' ').Replace('1', ' ').Replace('2', ' ').Replace('3', ' ').Replace('4', ' ').Replace('5', ' ').Replace('6', ' ').Replace('7', ' ').Replace('8', ' ')
                        .Replace('9', ' ').Replace('0', ' ').Replace('.', ' ').Replace('？', ' ').Replace('~',' ').Trim();


                    File.WriteAllText(path, content);
                }
            }

            ExecutedStatus = true;
            ExecutedMsg = "Generator All File Success";
        }

        public string PluginName
        {
            get
            {
                return "txt文本生成";
            }
        }

        public string WarningConfirmText
        {
            get
            {
                //return "你确认已经加载过文件吗？";
                return "";
            }
        }

        public string PluginAuthorInfo
        {
            get
            {
                //   return "插件作者信息";
                return "AKB需求 肖念";
            }
        }

        public string WorkBookName { get; set; }
        public IContext Context { get; set; }
        public bool ExecutedStatus { get; set; }
        public string ExecutedMsg { get; set; }
    }
}
