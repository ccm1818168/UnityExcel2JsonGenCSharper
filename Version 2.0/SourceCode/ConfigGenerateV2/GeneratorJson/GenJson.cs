﻿//
//    Copyright 2016 KeyleXiao.
//    Contact to Me : Keyle_xiao@hotmail.com 
//
//   	Licensed under the Apache License, Version 2.0 (the "License");
//   	you may not use this file except in compliance with the License.
//   	You may obtain a copy of the License at
//
//   		http://www.apache.org/licenses/LICENSE-2.0
//
//   		Unless required by applicable law or agreed to in writing, software
//   		distributed under the License is distributed on an "AS IS" BASIS,
//   		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   		See the License for the specific language governing permissions and
//   		limitations under the License.
//
using System.Collections.Generic;
using System.Text;
using IGeneratorContract;
using System;
using System.Linq;
using System.IO;

namespace GeneratorJson
{
    public class GenJson : IPluginContract
    {

        [MenuItem("json/GenJsonFile")]
        public void GenJsonFile()
        {
            foreach (var item in Context.ExcelInfos)
            {
                if (!item.Value)
                    continue;

                //string json = CreatSchema(item.Key.SheetName,"1.0");
                string tempJson = string.Empty;

                List<List<string>> list = item.Key.Rows.Values.ToList();

                for (int i = 0; i < list.Count; i++)
                {
                    tempJson += CreateItem(item.Key.ColumnType, item.Key.ColumnName, list[i], i != list.Count - 1);
                }


                //Write Path
                string path = Path.Combine(Context.CustomSetting.DefaultExportPath, item.Key.SheetName+".txt");
                if (File.Exists(path))
                    File.Delete(path);
                string json = CreatSchema(item.Key.SheetName, "1.0")+ tempJson+ CreatSchemaEnd(item.Key.SheetName, "1.0");
                File.WriteAllText(path, json);
            }

            ExecutedStatus = true;
            ExecutedMsg = "Build Success";
        }

        private string CreatSchema(string index,string version)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{");
            sb.Append("\"ConfigList\": [");
            //sb.Append("{");
            //sb.Append(" {0} ");
            //sb.Append("}");
            //sb.Append("],");
            //sb.Append(string.Format("\"Version\": \"{0}\",",version));
            //sb.Append(string.Format("\"Index\": \"{0}\"",index));
            //sb.Append("}");

            return sb.ToString();
        }

        private string CreatSchemaEnd(string index, string version)
        {
            StringBuilder sb = new StringBuilder();

            //sb.Append("{");
            //sb.Append("\"ConfigList\": [");
            //sb.Append("{");
            //sb.Append(" {0} ");
            //sb.Append("}");
            sb.Append("],");
            sb.Append(string.Format("\"Version\": \"{0}\",", version));
            sb.Append(string.Format("\"Index\": \"{0}\"", index));
            sb.Append("}");

            return sb.ToString();
        }



        public string CreateItem(List<Type> ColumnType, List<string> ColumnName ,List<string> currentRow, bool appendComma)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");

            for (int i = 0; i < currentRow.Count; i++)
            {
                sb.Append(string.Format("\"{0}\"",ColumnName[i]));
                sb.Append(":");
                if (ColumnType[i] == typeof(int))
                {
                    sb.Append(currentRow[i]);
                }
                if (ColumnType[i] == typeof(float))
                {
                    sb.Append(currentRow[i]);
                }
                if (ColumnType[i] == typeof(string))
                {
                    sb.Append(string.Format("\"{0}\"", currentRow[i]));
                }
                if (ColumnType[i] == typeof(int[]))
                {

                }
                if (ColumnType[i] == typeof(float[]))
                {

                }
                if (ColumnType[i] == typeof(string[]))
                {

                }

                if (i != currentRow.Count - 1)
                    sb.Append(",");
            }

            sb.Append("}");
            if (appendComma)
                sb.Append(",");

            return sb.ToString();
        }
     

        public string PluginName
        {
            get
            {
                return "Json生成";
            }
        }

        public string WarningConfirmText
        {
            get
            {
                //return "你确认已经加载过文件吗？";
                return "";
            }
        }

        public string PluginAuthorInfo
        {
            get
            {
                //   return "插件作者信息";
                return "";
            }
        }

        public string WorkBookName { get; set; }
        public IContext Context { get; set; }
        public bool ExecutedStatus { get; set; }
        public string ExecutedMsg { get; set; }
    }
}
